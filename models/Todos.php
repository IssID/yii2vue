<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int user_id
 * @property string $title
 * @property integer $completed
 */
class Todos extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%todos}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'completed'], 'required'],
            [['user_id'], 'integer'],
            [['completed'], 'boolean'],
            [['title'], 'string', 'max' => 255],
        ];
    }
}
