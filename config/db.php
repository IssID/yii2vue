<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . $_ENV['DB_MYSQL_HOST'] . ';dbname=' . $_ENV['DB_MYSQL_NAME'],
    'username' => $_ENV['DB_MYSQL_USER'],
    'password' => $_ENV['DB_MYSQL_PASSWORD'],
    'charset' => 'utf8mb4',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
