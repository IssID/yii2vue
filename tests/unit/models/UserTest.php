<?php

namespace tests\unit\models;

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    public function testFindUserById()
    {
        $user = User::findByLogin('admin');
        verify($user = User::findIdentity($user->id))->notEmpty();
        verify($user->login)->equals('admin');

        verify(User::findIdentity(999))->empty();
    }

    public function testFindUserByAccessToken()
    {
        $user = User::findByLogin('admin');
        verify($user = User::findIdentityByAccessToken($user->auth_key))->notEmpty();
        verify($user->login)->equals('admin');

        verify(User::findIdentityByAccessToken('non-existing'))->empty();        
    }

    public function testFindUserByLogin()
    {
        verify(User::findByLogin('admin'))->notEmpty();
        verify(User::findByLogin('not-admin'))->empty();
    }

    /**
     * @depends testFindUserByLogin
     */
    public function testValidateUser()
    {
        $user = User::findByLogin('admin');
        verify($user->validateAuthKey($user->auth_key))->notEmpty();
        verify($user->validateAuthKey('test102key'))->empty();

        verify($user->validatePassword('admin'))->notEmpty();
        verify($user->validatePassword('123456'))->empty();        
    }

}
