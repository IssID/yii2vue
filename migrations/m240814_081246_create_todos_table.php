<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%todos}}`.
 */
class m240814_081246_create_todos_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%todos}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'completed' => $this->boolean()->notNull()->defaultValue(false),
        ]);

        $this->createIndex(
            'idx-todos-user_id',
            'todos',
            'user_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-todos-user_id',
            'todos'
        );

        $this->dropTable('{{%todos}}');
    }
}
