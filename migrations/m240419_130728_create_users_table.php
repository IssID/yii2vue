<?php

use yii\db\Migration;

/**
 * Class m240419_130728_create_users_table
 */
class m240419_130728_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'login' => $this->char(64)->notNull()->unique(),
            'password' => $this->char(255)->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'auth_key' => $this->char(64),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
