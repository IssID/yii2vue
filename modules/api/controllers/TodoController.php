<?php

namespace app\modules\api\controllers;

use app\models\Todos;
use Yii;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

//todo вынести бизнес логику в сервисный слой
class TodoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['*'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['add'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['complete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['*'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionIndex() {
        if (!$identity = \Yii::$app->user->identity) {
            return [
                'success' => false,
                'errors' => ['User not found']
            ];
        }

        $models = Todos::find()->where(['user_id' => $identity->id])->all();

        return [
            'success' => true,
            'data' => $models
        ];
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionAdd() {
        if (!$identity = \Yii::$app->user->identity) {
            return [
                'success' => false,
                'errors' => ['User not found']
            ];
        }

        $model = new Todos();
        $data = Yii::$app->request->post();
        $data['user_id'] = $identity->id;
        $data['completed'] = false;

        if ($model->load($data, '') && $model->save()) {
            return [
                'success' => true,
                'data' => $model
            ];
        }

        return [
            'success' => false,
            'errors' => ['Can not add todo']
        ];
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionComplete() {
        if (!$identity = \Yii::$app->user->identity) {
            return [
                'success' => false,
                'errors' => ['User not found']
            ];
        }

        $data = Yii::$app->request->post();
        if ($model = Todos::find()->where(['user_id' => $identity->id])->andWhere(['id' => $data['id'] ?? null])->one()) {
            $model->completed = !$model->completed;

            if ($model->save()) {
                return [
                    'success' => true,
                    'data' => $model
                ];
            }
        }


        return [
            'success' => false,
            'errors' => $model
        ];
    }

    /**
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete() {
        if (!$identity = \Yii::$app->user->identity) {
            return [
                'success' => false,
                'errors' => ['User not found']
            ];
        }

        $data = Yii::$app->request->post();
        $model = Todos::find()->where(['user_id' => $identity->id])->andWhere(['id' => $data['id'] ?? null])->one();

        if ($model?->delete()) {
            return [
                'success' => true,
                'data' => $model
            ];
        }

        return [
            'success' => false,
            'errors' => $model
        ];
    }
}
